wp_kickstarter
==============

## Instalacja ##
Zainstaluj wszystkie pakiety za pomocą npm:
```
#!cmd

npm install
```
Zainstaluj obierz wszystkie frontowe vendory za pomocą Bowera:
```
#!cmd

bower install
```
Skopiuj pliki wszystkich vendorów do Twojego tematu. Polecenie to możesz powtórzyć za każdym razem kiedy będziesz potrzebować odświeżyć pliki vendorów. Pamiętaj, że pliki te będą wymagały wtedy commmit'u do GIT.
```
#!cmd

grunt copy-vendors
```
Developuj!
```
#!cmd

grunt watch
```