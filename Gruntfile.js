'use strict';
module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		meta: {
			version: '2.8.2',
			banner: '/*!\n' +
			' * <%= pkg.name %> v<%= pkg.version %>\n' +
			' * %= pkg.project-url %>\n *\n' +
			' * Date: <%= grunt.template.today("yyyy-mm-dd") %>\n *\n' +
			' * Copyright (c) Rawlplug\n */'
		},
		copy: {
			'vendors-js': {
				expand: true,
				flatten: true,
				src: [
					'bower_components/bootstrap/dist/js/bootstrap.min.js',
					'bower_components/jquery/dist/jquery.min.js',
					'bower_components/modernizer/modernizr.js'
				],
				dest: 'webroot/wp-content/themes/<%= pkg.name %>/js/lib/'
			},
			'vendors-css': {
				expand: true,
				flatten: true,
				src: 'bower_components/bootstrap/dist/css/*min.css',
				dest: 'webroot/wp-content/themes/<%= pkg.name %>/css/lib/'
			}
		},
		uglify: {
			options: {
				banner: '<%= meta.banner %>',
				preserveComments: 'some',
			},
			my_target: {
				files: {
					'webroot/wp-content/themes/<%= pkg.name %>/js/script.min.js': ['webroot/wp-content/themes/<%= pkg.name %>/js/src/*.js']
				}
			}
		},
		less: {
			development: {
				options: {
					cleancss: true
				},
				files: {
					'webroot/wp-content/themes/<%= pkg.name %>/css/main.css': 'webroot/wp-content/themes/<%= pkg.name %>/css/src/*.less'
				}
			}
		},
		watch: {
			scripts: {
				files: ['webroot/wp-content/themes/<%= pkg.name %>/js/src/*.js', 'webroot/wp-content/themes/<%= pkg.name %>/css/src/*.less'],
				tasks: ['uglify', 'less']
			}
		}
	});

	// Ładowanie zadania
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');

	grunt.registerTask('copy-vendors', ['copy:vendors-js', 'copy:vendors-css']);

	// Wystarczy wywoła z linii poleceń 'grunt'. Domyślnym taskiem jest 'watch'
	grunt.registerTask('default', ['watch']);
};
